#
#  Copyright (C) 2016, Marvell International Ltd. ALL RIGHTS RESERVED.
#*
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
#
#    THIS CODE IS PROVIDED ON AN  *AS IS* BASIS, WITHOUT WARRANTIES OR
#    CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT
#    LIMITATION ANY IMPLIED WARRANTIES OR CONDITIONS OF TITLE, FITNESS
#    FOR A PARTICULAR PURPOSE, MERCHANTABILITY OR NON-INFRINGEMENT.
#
#    See the Apache Version 2.0 License for specific language governing
#    permissions and limitations under the License.
#
"""
topology_docker_marvellswitch module entry point.
"""

from __future__ import unicode_literals, absolute_import
from __future__ import print_function, division

__author__ = ''
__email__ = 'aslitsky@larch-networks.com'
__version__ = '0.1.0'

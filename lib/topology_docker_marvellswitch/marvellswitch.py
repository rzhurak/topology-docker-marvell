# -*- coding: utf-8 -*-
#
# Copyright (C) 2015-2016 Hewlett Packard Enterprise Development LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.

"""
Custom Topology Docker Node for MarvellSwitch.
"""

from __future__ import unicode_literals, absolute_import
from __future__ import print_function, division

from json import loads
from subprocess import check_call

from topology_docker.node import DockerNode
from topology_docker.shell import DockerShell, DockerBashShell

WAIT_FPA_SCRIPT = """\
TIMEOUT=$1

re='^[0-9]+$'
if ! [[ $TIMEOUT =~ $re ]] ; then
    echo "Error: Not a number" >&2; exit 1
fi

SPENT_TIME=1
while (true);
do 
   sleep 1
   SPENT_TIME=`expr $SPENT_TIME + 1`

   if [ -f '/var/run/fpa-sim-init.done' ]; then
       echo 'Pass. FPA simulator started'
       exit 0
   fi
   if [ "$SPENT_TIME" -ge "$TIMEOUT" ]; then
       echo 'Error. Timeout starting FPA simulator'
       exit 0
   fi

done

"""

SETUP_SCRIPT = """\
import logging
from sys import argv
from time import sleep
from os.path import exists, split
from json import dumps, loads
from shlex import split as shsplit
from subprocess import check_call, check_output
from socket import AF_UNIX, SOCK_STREAM, socket, gethostname

import yaml

config_timeout = 300
swns_netns = '/var/run/netns/swns'
hwdesc_dir = '/etc/openswitch/hwdesc'
db_sock = '/var/run/openvswitch/db.sock'
switchd_pid = '/var/run/openvswitch/ops-switchd.pid'
query = {
    'method': 'transact',
    'params': [
        'OpenSwitch',
        {
            'op': 'select',
            'table': 'System',
            'where': [],
            'columns': ['cur_hw']
        }
    ],
    'id': id(db_sock)
}
sock = None


def create_interfaces():
    # Read ports from hardware description
    with open('{}/ports.yaml'.format(hwdesc_dir), 'r') as fd:
        ports_hwdesc = yaml.load(fd)
    
    hwports = [(str(p['name']), str(p['slan'])) for p in ports_hwdesc['ports']]
    #hwports = [str(p['name']) for p in ports_hwdesc['ports']]
    # Get list of already created ports
    not_in_swns = check_output(shsplit(
        'ls /sys/class/net/'
    )).split()
    logging.info('yaml {}'.format(ports_hwdesc))
    in_swns = check_output(shsplit(
        'ip netns exec swns ls /sys/class/net/'
    )).split()
    logging.info(
            '  - Not in swns {not_in_swns} '.format(
                **locals()
            )
        )
    logging.info(
            '  - In swns {in_swns} '.format(
                **locals()
            )
        )

    create_cmd_tpl = 'ip tuntap add dev {hwport} mode tap'
    netns_cmd_tpl = 'ip link set {hwport} netns swns'
    rename_int = 'ip link set {portlbl} name {hwport[1]}'
    run_slan_eth = 'ip netns exec swns /opt/marvell-sw/slanEthPortLinux -D -n {slan_name} {port_name}'
    linkup_cmd_tpl = 'ip netns exec swns ip link set {hwport[1]} up'
    
    # Save port mapping information
    mapping_ports = {}

    # Map the port with the labels
    for portlbl in not_in_swns:
        logging.info(
            '  - Port {portlbl} found'.format(
                **locals()
            )
        )
        if portlbl in ['lo', 'oobm', 'eth0', 'bonding_masters']:
            continue
        hwport = hwports.pop(0)
        mapping_ports[portlbl] = hwport[0]
        logging.info(
            '  - Port {portlbl} moved to swns netns as {hwport[1]}.'.format(
                **locals()
            )
        )
        logging.info(
            '  Running slan with slan={} hwport={}'.format(
                hwport[1], hwport[1]
            )
        )
        try:
            check_call(shsplit(rename_int.format(**locals())))
            check_call(shsplit(netns_cmd_tpl.format(hwport=hwport[1])))
            logging.info(linkup_cmd_tpl.format(**locals()));
            check_call(shsplit(linkup_cmd_tpl.format(**locals())))
            logging.info(run_slan_eth.format(slan_name=hwport[1],port_name=hwport[1]))
            check_call(shsplit(run_slan_eth.format(slan_name=hwport[1],port_name=hwport[1])))
        except:
            raise Exception('Failed to map ports with port labels')

    # Writting mapping to file
    shared_dir_tmp = split(__file__)[0]
    with open('{}/port_mapping.json'.format(shared_dir_tmp), 'w') as json_file:
        json_file.write(dumps(mapping_ports))

#    for hwport in hwports:
#        if hwport in in_swns:
#            logging.info('  - Port {} already present.'.format(hwport))
#            continue
#
#        logging.info('  - Port {} created.'.format(hwport[1]))
#        try:
#            check_call(shsplit(create_cmd_tpl.format(hwport=hwport[1])))
#        except:
#            raise Exception('Failed to create tuntap')
#
#        try:
#            check_call(shsplit(netns_cmd_tpl.format(hwport=hwport[1])))
#        except:
#            raise Exception('Failed to move port to swns netns')
    check_call(shsplit('touch /tmp/ops-virt-ports-ready'))
    logging.info('  - Ports readiness notified to the image')

def cur_cfg_is_set():
    global sock
    if sock is None:
        sock = socket(AF_UNIX, SOCK_STREAM)
        sock.connect(db_sock)
    sock.send(dumps(query))
    response = loads(sock.recv(4096))
    try:
        return response['result'][0]['rows'][0]['cur_hw'] == 1
    except IndexError:
        return 0

def main():

    if '-d' in argv:
        logging.basicConfig(level=logging.DEBUG)

    logging.info('Waiting for swns netns...')
    for i in range(0, config_timeout):
        if not exists(swns_netns):
            sleep(0.1)
        else:
            break
    else:
        raise Exception('Timed out while waiting for swns.')

    logging.info('Waiting for hwdesc directory...')
    for i in range(0, config_timeout):
        if not exists(hwdesc_dir):
            sleep(0.1)
        else:
            break
    else:
        raise Exception('Timed out while waiting for hwdesc directory.')

    logging.info('Creating interfaces...')
    create_interfaces()

    logging.info('Waiting for DB socket...')
    for i in range(0, config_timeout):
        if not exists(db_sock):
            sleep(0.1)
        else:
            break
    else:
        raise Exception('Timed out while waiting for DB socket.')

    logging.info('Waiting for switchd pid...')
    for i in range(0, config_timeout):
        if not exists(switchd_pid):
            sleep(0.1)
        else:
            break
    else:
        raise Exception('Timed out while waiting for switchd pid.')

    logging.info('Wait for final hostname...')
    for i in range(0, config_timeout):
        if gethostname() != 'switch':
            sleep(0.1)
        else:
            break
    else:
        raise Exception('Timed out while waiting for final hostname.')

    logging.info('Waiting for cur_cfg...')
    for i in range(0, config_timeout):
        if not cur_cfg_is_set():
            sleep(0.1)
        else:
            break
    else:
        raise Exception('Timed out while waiting for cur_cfg.')

if __name__ == '__main__':
    main()
"""


PROCESS_LOG = """
#!/bin/bash
ovs-vsctl list Daemon >> /tmp/logs
echo "Coredump -->" >> /tmp/logs
coredumpctl gdb >> /tmp/logs
echo "All the running processes:" >> /tmp/logs
ps -aef >> /tmp/logs

systemctl status >> /tmp/systemctl
systemctl --state=failed --all >> /tmp/systemctl

ovsdb-client dump >> /tmp/ovsdb_dump
"""


class MarvellSwitchNode(DockerNode):
    """
    Custom MarvellSwitch node for the Topology Docker platform engine.

    This custom node loads an MarvellSwitch image and has vtysh as default
    shell (in addition to bash).

    See :class:`topology_docker.node.DockerNode`.
    """

    def __init__(
            self, identifier,
            image='topology/marvell:latest', binds=None,
            **kwargs):

        # Add binded directories
        container_binds = [
            '/dev/log:/dev/log',
            '/sys/fs/cgroup:/sys/fs/cgroup:ro'
        ]
        if binds is not None:
            container_binds.append(binds)
        
        super(MarvellSwitchNode, self).__init__(
            identifier, image=image, command='/sbin/init',
            binds=';'.join(container_binds), hostname='switch',
            network_mode='bridge', **kwargs
        )

        # FIXME: Remove this attribute to merge with version > 1.6.0
        self.shared_dir_mount = '/tmp'

        # Add vtysh (default) shell
        # FIXME: Create a subclass to handle better the particularities of
        # vtysh, like prompt setup etc.
        self._shells['vtysh'] = DockerShell(
            self.container_id, 'vtysh', '(^|\n)switch(\([\-a-zA-Z0-9]*\))?#'
        )

        # Add bash shells
        initial_prompt = '(^|\n).*[#$] '

        self._shells['bash'] = DockerBashShell(
            self.container_id, 'bash',
            initial_prompt=initial_prompt
        )
        self._shells['bash_swns'] = DockerBashShell(
            self.container_id, 'ip netns exec swns bash',
            initial_prompt=initial_prompt
        )
        self._shells['vsctl'] = DockerBashShell(
            self.container_id, 'bash',
            initial_prompt=initial_prompt,
            prefix='ovs-vsctl ', timeout=60
        )

    def notify_post_build(self):
        """
        Get notified that the post build stage of the topology build was
        reached.

        See :meth:`DockerNode.notify_post_build` for more information.
        """
        super(MarvellSwitchNode, self).notify_post_build()
        self._setup_system()

    def _setup_system(self):
        """
        Setup the MarvellSwitch image for testing.

        #. Wait for daemons to converge.
        #. Assign an interface to each port label.
        #. Create remaining interfaces.
        """

        # Write the log gathering script
        process_log = '{}/process_log.sh'.format(self.shared_dir)
        with open(process_log, "w") as fd:
            fd.write(PROCESS_LOG)
        check_call('chmod 755 {}/process_log.sh'.format(self.shared_dir),
                   shell=True)

        # Write and execute setup script
        setup_script = '{}/marvellswitch_setup.py'.format(self.shared_dir)
        with open(setup_script, 'w') as fd:
            fd.write(SETUP_SCRIPT)

        wait_fpa_script = '{}/wait_fpa_run.sh'.format(self.shared_dir)
        with open(wait_fpa_script, 'w') as fd:
            fd.write(WAIT_FPA_SCRIPT)

        try:
            self._docker_exec('python {}/marvellswitch_setup.py -d'
                              .format(self.shared_dir_mount))
            print("Waiting for FPA init done...")
            out = self._docker_exec('bash {}/wait_fpa_run.sh 42'
                              .format(self.shared_dir_mount))
            print("Init done with output: {}".format(out))
            if "Error" in out:
                raise Exception("Error. Fpa simulator not started")
        except Exception as e:
            check_call('touch {}/logs'.format(self.shared_dir), shell=True)
            check_call('chmod 766 {}/logs'.format(self.shared_dir),
                       shell=True)
            self._docker_exec('/bin/bash {}/process_log.sh'
                              .format(self.shared_dir_mount))
            check_call(
                'tail -n 2000 /var/log/syslog > {}/syslog'.format(
                    self.shared_dir
                ), shell=True)
            check_call(
                'docker ps -a >> {}/logs'.format(self.shared_dir),
                shell=True
            )
            check_call('cat {}/logs'.format(self.shared_dir), shell=True)
            raise e

        # Read back port mapping
        port_mapping = '{}/port_mapping.json'.format(self.shared_dir)
        with open(port_mapping, 'r') as fd:
            mappings = loads(fd.read())

        if hasattr(self, 'ports'):
            self.ports.update(mappings)
            return
        self.ports = mappings

    def set_port_state(self, portlbl, state):
        """
        Set the given port label to the given state.

        See :meth:`DockerNode.set_port_state` for more information.
        """
        iface = self.ports[portlbl]
        state = 'up' if state else 'down'

        not_in_netns = self._docker_exec('ls /sys/class/net/').split()
        prefix = '' if iface in not_in_netns else 'ip netns exec swns'

        command = '{prefix} ip link set dev {iface} {state}'.format(**locals())
        self._docker_exec(command)


__all__ = ['MarvellSwitchNode']
